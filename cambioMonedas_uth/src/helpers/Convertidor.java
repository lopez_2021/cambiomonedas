package helpers;

import java.text.DecimalFormat;

public class Convertidor {
    DecimalFormat formato = new DecimalFormat("#.##");
    public double convertirValor(double valor, String origen, String destino){
        double cambio=0;
        //convertir lempiras a otras denominaciones
        if(origen.equalsIgnoreCase("Lempira") && destino.equalsIgnoreCase("Lempira")){
            cambio=valor*1;
        }else  if(origen.equalsIgnoreCase("Lempira") && destino.equalsIgnoreCase("Dolar")){
            cambio=valor*0.040;
        }else  if(origen.equalsIgnoreCase("Lempira") && destino.equalsIgnoreCase("Euro")){
            cambio=valor*0.034;
        }else  if(origen.equalsIgnoreCase("Lempira") && destino.equalsIgnoreCase("Quetzal")){
            cambio=valor*0.31;
        }else  if(origen.equalsIgnoreCase("Lempira") && destino.equalsIgnoreCase("Soles")){
            cambio=valor*0.14;
        }
        //convertir dolar a otras denominaciones
                if(origen.equals("Dolar") && destino.equalsIgnoreCase("Lempira")){
            cambio=valor*24.91;
        }else  if(origen.equals("Dolar") && destino.equalsIgnoreCase("Dolar")){
            cambio=valor*1;
        }else  if(origen.equals("Dolar") && destino.equalsIgnoreCase("Euro")){
            cambio=valor*0.85;
        }else  if(origen.equals("Dolar") && destino.equalsIgnoreCase("Quetzal")){
            cambio=valor*7.77;
        }else  if(origen.equals("Dolar") && destino.equalsIgnoreCase("Soles")){
            cambio=valor*3.54;
        }
        //Convertir de euro a otras denominaciones
       if(origen.equals("Euro") && destino.equalsIgnoreCase("Lempira")){
            cambio=valor*29.31;
        }else  if(origen.equals("Euro") && destino.equalsIgnoreCase("Dolar")){
            cambio=valor*1.18;
        }else  if(origen.equals("Euro") && destino.equalsIgnoreCase("Euro")){
            cambio=valor*1;
        }else  if(origen.equals("Euro") && destino.equalsIgnoreCase("Quetzal")){
            cambio=valor*9.14;
        }else  if(origen.equals("Euro") && destino.equalsIgnoreCase("Soles")){
            cambio=valor*4.17;
        }
       //Convertir Quetzar a otras denominaciones
              if(origen.equals("Quetzal") && destino.equalsIgnoreCase("Lempira")){
            cambio=valor*3.21;
        }else  if(origen.equals("Quetzal") && destino.equalsIgnoreCase("Dolar")){
            cambio=valor*0.13;
        }else  if(origen.equals("Quetzal") && destino.equalsIgnoreCase("Euro")){
            cambio=valor*.11;
        }else  if(origen.equals("Quetzal") && destino.equalsIgnoreCase("Quetzal")){
            cambio=valor*1;
        }else  if(origen.equals("Quetzal") && destino.equalsIgnoreCase("Soles")){
            cambio=valor*0.45;
        }
              //convertir soles a otras denominaciones
        if(origen.equals("Soles") && destino.equalsIgnoreCase("Lempira")){
            cambio=valor*7.04;
        }else  if(origen.equals("Soles") && destino.equalsIgnoreCase("Dolar")){
            cambio=valor*.28;
        }else  if(origen.equals("Soles") && destino.equalsIgnoreCase("Euro")){
            cambio=valor*.24;
        }else  if(origen.equals("Soles") && destino.equalsIgnoreCase("Quetzal")){
            cambio=valor*2.19;
        }else  if(origen.equals("Soles") && destino.equalsIgnoreCase("Soles")){
            cambio=valor*1;
        }
        String total = formato.format(cambio);
        return Double.parseDouble(total);
    }
    public static void mensaje(int estado, String mensaje){
        javax.swing.JOptionPane.showMessageDialog(null, mensaje,"Nota",estado);
    }
}
